<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Factory\Model;

use Nfq\DpdClient\Model\ParcelMachine;

class ParcelMachineFactory
{
    /**
     * @param array $data
     *
     * @return ParcelMachine
     */
    public static function create(array $data): ParcelMachine
    {
        $parcelMachine = new ParcelMachine();

        $parcelMachine
            ->setId($data['parcelshop_id'])
            ->setCity($data['city'])
            ->setStreet($data['street'])
            ->setCountry($data['country'])
            ->setCompany($data['company'])
            ->setPostCode($data['pcode'])
            ->setLongitude($data['longitude'])
            ->setLatitude($data['latitude']);

        return $parcelMachine;
    }

    /**
     * @param array $data
     *
     * @return ParcelMachine[]
     */
    public static function fromResponse(array $data): array
    {
        $parcelMachines = [];

        foreach ($data['parcelshops'] as $datum) {
            $parcelMachines[] = self::create($datum);
        }

        return $parcelMachines;
    }
}
