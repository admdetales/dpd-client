<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\Factory\Model\Tracking;

use Nfq\DpdClient\Model\Tracking\Tracking;

class TrackingFactory
{
    /**
     * @param array $data
     *
     * @return Tracking
     */
    public static function create(array $data): Tracking
    {
        $details = $data['details'] ? DetailFactory::fromResponse($data['details']) : [];
        $error = $data['error'] ? ErrorFactory::fromResponse($data['error']) : null;

        $tracking = new Tracking();
        $tracking
            ->setParcelNumber($data['parcelNumber'])
            ->setDetails($details)
            ->setError($error);

        return $tracking;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    public static function fromResponse(array $data): array
    {
        $trackings = [];

        foreach ($data as $key => $value) {
            $trackings[] = self::create($value);
        }

        return $trackings;
    }
}
