<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Nfq\DpdClient\HttpClient;

use Http\Client\HttpClient;
use Http\Discovery\MessageFactoryDiscovery;
use Http\Message\MessageFactory;
use Nfq\DpdClient\Exception\BadRequestException;
use Nfq\DpdClient\Exception\InvalidResponseException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

class Client
{
    private const STATUS_ERROR = 'err';

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var MessageFactory|null
     */
    private $messageFactory;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param string $path
     * @param array $body
     *
     * @return array|StreamInterface
     */
    public function post(string $path, array $body = [])
    {
        return $this->sendRequest('POST', $path, [], http_build_query($body));
    }

    /**
     * @param string $path
     * @param array $body
     *
     * @return array|StreamInterface
     */
    public function get(string $path, array $body = [])
    {
        return $this->sendRequest('GET', $path, [], http_build_query($body));
    }

    private function sendRequest(string $method, string $path, array $headers = [], string $body = null)
    {
        $request = $this->getMessageFactory()->createRequest($method, $path, $headers, $body);
        $response = $this->httpClient->sendRequest($request);

        if ($response->hasHeader('Content-Type')) {
            foreach ($response->getHeader('Content-Type') as $contentType) {
                if (false !== strpos($contentType, 'application/json')) {
                    return $this->parseJsonResponse($response);
                }
            }
        }

        return $response->getBody();
    }

    private function parseJsonResponse(ResponseInterface $response): array
    {
        $rawData = (string) $response->getBody();

        if (empty($rawData)) {
            throw new InvalidResponseException('The server returned an empty response');
        }

        $data = json_decode($rawData, true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidResponseException('Could not decode the response.');
        }

        if (isset($data['status']) && self::STATUS_ERROR === $data['status']) {
            throw new BadRequestException('The server returned an error: ' . $data['errlog'] ?? '');
        }

        return $data;
    }

    private function getMessageFactory(): MessageFactory
    {
        if ($this->messageFactory === null) {
            $this->messageFactory = MessageFactoryDiscovery::find();
        }

        return $this->messageFactory;
    }
}
