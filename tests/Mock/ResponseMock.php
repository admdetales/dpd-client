<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Nfq\DpdClient\Mock;

use PHPUnit\Framework\MockObject\Generator;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\ResponseInterface;

class ResponseMock
{
    /**
     * @param string $filename
     *
     * @return ResponseInterface|MockObject
     *
     * @throws \ReflectionException
     */
    public static function fromFile(string $filename): ResponseInterface
    {
        $response = self::createMock();
        $response = self::addContentType($response);

        $response->method('getBody')->willReturn(
            file_get_contents(__DIR__ . '/../Responses/' . $filename)
        );

        return $response;
    }

    /**
     * @param array $data
     *
     * @return ResponseInterface|MockObject
     *
     * @throws \ReflectionException
     */
    public static function fromArray(array $data): ResponseInterface
    {
        $response = self::createMock();
        $response = self::addContentType($response);

        $response
            ->method('getBody')
            ->willReturn(json_encode($data));

        return $response;
    }

    public static function fromString(string $string): ResponseInterface
    {
        $response = self::createMock();
        $response = self::addContentType($response);

        $response
            ->method('getBody')
            ->willReturn($string);

        return $response;
    }

    public static function createMock(): ResponseInterface
    {
        return (new Generator())->getMock(ResponseInterface::class);
    }

    private static function addContentType(ResponseInterface $response): ResponseInterface
    {
        $response
            ->method('hasHeader')
            ->with('Content-Type')
            ->willReturn(true);

        $response
            ->method('getHeader')
            ->with('Content-Type')
            ->willReturn(['application/json;charset=UTF-8']);

        return $response;
    }
}
