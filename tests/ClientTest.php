<?php

/*
 * @copyright C UAB NFQ Technologies
 *
 * This Software is the property of NFQ Technologies
 * and is protected by copyright law – it is NOT Freeware.
 *
 * Any unauthorized use of this software without a valid license key
 * is a violation of the license agreement and will be prosecuted by
 * civil and criminal law.
 *
 * Contact UAB NFQ Technologies:
 * E-mail: info@nfq.lt
 * http://www.nfq.lt
 */

declare(strict_types=1);

namespace Tests\Nfq\DpdClient;

use GuzzleHttp\Psr7\Stream;
use Http\Mock\Client as MockClient;
use Nfq\DpdClient\Client as DpdClient;
use Nfq\DpdClient\Constants\PrintFormats;
use Nfq\DpdClient\Constants\PrintSizes;
use Nfq\DpdClient\Constants\ServiceCodes;
use Nfq\DpdClient\HttpClient\Client as HttpClient;
use Nfq\DpdClient\HttpClient\ClientFactory;
use Nfq\DpdClient\HttpClient\StatusFactory;
use Nfq\DpdClient\Request\CheckTrackingRequest;
use Nfq\DpdClient\Request\CreateParcelLabelRequest;
use Nfq\DpdClient\Request\CreateShipmentRequest;
use Nfq\DpdClient\Request\SearchParcelMachineRequest;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\ResponseInterface;
use Tests\Nfq\DpdClient\Mock\ResponseMock;

class ClientTest extends TestCase
{
    /**
     * @var DpdClient
     */
    private $client;

    /**
     * @var MockClient
     */
    private $mockClient;

    /**
     * @var HttpClient
     */
    private $httpClient;

    /**
     * @var string
     */
    private $trackingCode = '13409600123862';

    protected function setUp()
    {
        $this->mockClient = new MockClient();

        $this->httpClient = new \Http\Adapter\Guzzle6\Client(
            new \GuzzleHttp\Client(['curl' => [CURLOPT_SSL_VERIFYPEER => false]])
        );

        $this->client = new DpdClient(
            new HttpClient(ClientFactory::create('testuser1', 'testpassword1', true, $this->mockClient)),
            new HttpClient(StatusFactory::create($this->httpClient))
        );
    }

    public function testGetParcelShopsReturnsParcelMachineModels()
    {
        $response = ResponseMock::fromFile('parcel-machines-success.json');

        $this->mockClient->addResponse($response);
        $parcelMachines = $this->client->getParcelMachines(new SearchParcelMachineRequest('LT'));
        $parcelMachine = reset($parcelMachines);

        $this->assertCount(2, $parcelMachines);

        $this->assertEquals('LT10125', $parcelMachine->getId());
        $this->assertEquals('VAGA BABILONAS siuntų taškas', $parcelMachine->getCompany());
        $this->assertEquals('LT', $parcelMachine->getCountry());
        $this->assertEquals('Panevėžys', $parcelMachine->getCity());
        $this->assertEquals('37189', $parcelMachine->getPostCode());
        $this->assertEquals('Klaipėdos g. 143A', $parcelMachine->getStreet());
        $this->assertEquals(24.313027, $parcelMachine->getLongitude());
        $this->assertEquals(55.730886, $parcelMachine->getLatitude());
    }

    public function testGetParcelMachinesSendsRequiredParameters()
    {
        $response = ResponseMock::fromFile('parcel-machines-success.json');

        $this->mockClient->addResponse($response);
        $this->client->getParcelMachines(new SearchParcelMachineRequest('LT'));

        $request = $this->mockClient->getLastRequest();

        $this->assertEquals('country=LT&fetchGsPUDOpoint=1', (string) $request->getBody());
    }

    public function testCreateShipmentWithB2bCodType()
    {
        $response = ResponseMock::fromFile('create-shipment-success.json');
        $this->mockClient->addResponse($response);

        $createShipmentRequest = $this->createShipmentRequest(ServiceCodes::STANDARD_PARCEL_B2C_COD);
        $createShipmentRequest->setCodAmount(15.99);

        $parcelNumberList = $this->client->createShipment($createShipmentRequest);
        $request = $this->mockClient->getLastRequest();

        $expected = 'name1=Jonas&street=Liepkalnio+g.+180-1&city=Vilnius&country=LT&pcode=02121&num_of_parcel=1'
                  . '&weight=1.5&parcel_type=D-B2C-COD&phone=%2B37065111111&cod_amount=15.99';

        $this->assertEquals($expected, (string) $request->getBody());
        $this->assertEquals(['05809023014739'], $parcelNumberList->getNumbers());
    }

    public function testCreateShipmentWithParcelshopShipmentType()
    {
        $response = ResponseMock::fromFile('create-shipment-success.json');
        $this->mockClient->addResponse($response);

        $createShipmentRequest = $this->createShipmentRequest(ServiceCodes::STANDARD_PARCEL_PARCEL_SHOP);
        $createShipmentRequest->setParcelShopId('LT10008');

        $this->client->createShipment($createShipmentRequest);
        $request = $this->mockClient->getLastRequest();

        $expected = 'name1=Jonas&street=Liepkalnio+g.+180-1&city=Vilnius&country=LT&pcode=02121&num_of_parcel=1'
            . '&weight=1.5&parcel_type=PS&parcelshop_id=LT10008&phone=%2B37065111111&fetchGsPUDOpoint=1';

        $this->assertEquals($expected, (string) $request->getBody());
    }

    public function testCreateParcelLabel()
    {
        $response = $this->createMock(ResponseInterface::class);

        $stream = fopen('php://memory', 'r+');
        fwrite($stream, 'pdf_content');

        $response
            ->method('getBody')
            ->willReturn(new Stream($stream));

        $this->mockClient->addResponse($response);

        $createParcelLabelRequest = new CreateParcelLabelRequest();
        $createParcelLabelRequest
            ->setParcelNumbers(['1', '2'])
            ->setPrintFormat(PrintFormats::FORMAT_ZPL)
            ->setPrintSize(PrintSizes::SIZE_A5);

        $this->client->createParcelLabel($createParcelLabelRequest);
        $request = $this->mockClient->getLastRequest();

        $this->assertEquals('parcels=1%7C2&printType=zpl&printFormat=A5', (string) $request->getBody());
    }

    private function createShipmentRequest(string $type): CreateShipmentRequest
    {
        $createShipmentRequest = new CreateShipmentRequest();

        $createShipmentRequest
            ->setName('Jonas')
            ->setStreet('Liepkalnio g. 180-1')
            ->setCity('Vilnius')
            ->setCountry('LT')
            ->setPostCode('02121')
            ->setPhone('+37065111111')
            ->setWeight(1.5)
            ->setNumberOfParcels(1)
            ->setParcelType($type);

        return $createShipmentRequest;
    }

    public function testCheckTracking()
    {
        $createTrackingRequest = new CheckTrackingRequest($this->trackingCode);

        $trackings = $this->client->getTrackings($createTrackingRequest);
        $tracking = reset($trackings);

        $trackingDetails = $tracking->getDetails();
        $trackingDetail = reset($trackingDetails);

        $this->assertCount(1, $trackings);
        $this->assertEquals($this->trackingCode, $tracking->getParcelNumber());
        $this->assertNull($tracking->getError());
        $this->assertEquals(false, $tracking->hasError());

        $this->assertCount(1, $trackingDetails);
        $this->assertEquals('Delivered to Consignee', $trackingDetail->getStatus());
    }

    /**
     * @expectedException Nfq\DpdClient\Exception\InvalidResponseException
     */
    public function testCheckTrackingError()
    {
        $createTrackingRequest = new CheckTrackingRequest($this->trackingCode . '1');

        $trackings = $this->client->getTrackings($createTrackingRequest);
        $tracking = reset($trackings);

        $trackingError = $tracking->getError();

        $this->assertEquals(true, $tracking->hasError());

        $this->assertEquals(400, $trackingError->getCode());
        $this->assertEquals('Invalid parcel number. Allowed: 14 chars, numeric.', $trackingError->getMessage());
    }

    public function testCheckTrackingWithDetail()
    {
        $createTrackingRequest = new CheckTrackingRequest($this->trackingCode);
        $createTrackingRequest->setDetail(true);

        $trackings = $this->client->getTrackings($createTrackingRequest);
        $tracking = reset($trackings);

        $trackingDetails = $tracking->getDetails();
        $trackingDetail = reset($trackingDetails);

        $this->assertCount(1, $trackings);
        $this->assertEquals($this->trackingCode, $tracking->getParcelNumber());
        $this->assertNull($tracking->getError());

        $this->assertCount(1, $trackingDetails);
        $this->assertEquals('13', $trackingDetail->getStatusCode());
        $this->assertEquals('101', $trackingDetail->getServiceCode());
    }

    public function testCheckTrackingWithShowAll()
    {
        $createTrackingRequest = new CheckTrackingRequest($this->trackingCode);
        $createTrackingRequest->setShowAll(true);

        $trackings = $this->client->getTrackings($createTrackingRequest);
        $tracking = reset($trackings);

        $trackingDetails = $tracking->getDetails();

        $this->assertCount(1, $trackings);
        $this->assertCount(6, $trackingDetails);
    }
}
